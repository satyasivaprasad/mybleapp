package com.example.bluetoothlibrary.entity

object Constant {
    const val BLT_WBP = "BLT_WBP"
    const val AL_WBP = "AL_WBP"
    const val BLT_WF1 = "BLT_WF1"
}