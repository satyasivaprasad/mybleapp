package com.example.bluetoothlibrary.entity

class ConnectBleServiceInfo {
    var deviceName: String? = null
    var serviceUUID: String? = null
    var charateUUID: String? = null
    var charateReadUUID: String? = null
    var charateALiRealTimeUUID: String? = null
    var charateALiBatteryUUID: String? = null
    var charateALiHistoryDataUUID: String? = null
    lateinit var conectModel: ByteArray
    override fun toString(): String {
        return ("bp: deviceName: " + deviceName
                + ", serviceUUID: " + serviceUUID
                + ", charateUUID: " + charateUUID
                + ", charateReadUUID: " + charateReadUUID
                + ", charateALiRealTimeUUID" + charateALiRealTimeUUID)
    }
}