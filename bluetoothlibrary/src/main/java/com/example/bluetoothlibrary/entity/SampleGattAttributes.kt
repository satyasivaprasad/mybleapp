/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.bluetoothlibrary.entity

import java.util.*

/**
 * This class includes a small subset of standard GATT attributes for
 * demonstration purposes.
 */
object SampleGattAttributes {
    @JvmField
    var SeviceIDfbb0 = "0000fff0-0000-1000-8000-00805f9b34fb"
    @JvmField
    var GetCharacteristicIDfbb2 = "0000fff4-0000-1000-8000-00805f9b34fb"
    @JvmField
    var GetCharacteristicIDfbb1 = "0000fff1-0000-1000-8000-00805f9b34fb"
    @JvmField
    var CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb"
    @JvmField
    var data = byteArrayOf(0xAA.toByte(), 0x06, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00) // ���β���
    @JvmField
    var data2 = byteArrayOf(0xAA.toByte(), 0x06, 0x78, 0x08, 0x00, 0x00, 0x00, 0x00) // ��������
    @JvmField
    var data4 = byteArrayOf(0xAA.toByte(), 0x05, 0x7A.toByte(), 0x00, 0x00, 0x00, 0x00) // 7.2.5.����ʼ������ʷ����(ID:122)
    @JvmField
    var data6 = byteArrayOf(0xAA.toByte(), 0x04, 0x7F.toByte(), 0x00, 0x00, 0x00) // 7.2.7.�����������ݣ�ID:127��
    @JvmField
    var data7 = byteArrayOf(0xAA.toByte(), 0x03, 0x79.toByte(), 0x00, 0x00)
    @JvmField
    var listdata: ArrayList<BleData> = ArrayList()
    @JvmStatic
    fun resendBleData(b1: Int, b2: Int, b3: Int, b4: Int): ByteArray {
        return byteArrayOf(0xAA.toByte(), 0x07, 0x7B.toByte(), b1.toByte(), b2.toByte(), b3.toByte(), b4.toByte(), 0x00, 0x00)
    }
}