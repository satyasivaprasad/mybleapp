package com.example.bluetoothlibrary.entity

import java.io.Serializable

class Peripheral : Serializable {
    var id = 0
    var name //
            : String? = null
    var brand //Ʒ��
            : String? = null
    var model //�ͺ�
            : String? = null
    var icon: String? = null
    var manufacturer //������
            : String? = null
    var bluetooth //����.m70c Bltwbp
            : String? = null
    var boundId = 0
    var memberID = 0
    var preipheralSN //���к�
            : String? = null
    var remark: String? = null
    var protocolVer = 0f
    var firmwareVer //�̼��汾
            = 0f
    var subVersion //�ӹ̼��汾.Ѫ��ר��
            : String? = null
    var preipheralMAC //Ӳ����ַ
            : String? = null
    var recordDate: String? = null
    var createdDate: String? = null
    var updatedDate: String? = null
    var isActivation = 0
        get() = field
    var webMode //Ѫѹ����ר��
            = 0

    override fun toString(): String {
        return "llllllllllllllllllllllll: name: $name, preipheralSN: $preipheralSN, model: $model, preipheralMAC: $preipheralMAC, protocolVer:$protocolVer"
    }
}