package com.example.bluetoothlibrary.Impl;

import android.app.Activity;
import android.util.Log;

import com.example.bluetoothlibrary.BluetoothLeClass;
import com.example.bluetoothlibrary.Interface.Wbpdata;
import com.example.bluetoothlibrary.R;
import com.example.bluetoothlibrary.SettingUtil;
import com.example.bluetoothlibrary.entity.BleData;
import com.example.bluetoothlibrary.entity.SampleGattAttributes;
import com.example.bluetoothlibrary.entity.SycnBp;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.bluetoothlibrary.SettingUtil.isGusetmode;

/**
 * Created by laiyiwen on 2017/4/28.
 */

public class ResolveWbp implements Wbpdata {
    public static int WBPMODE = -1;

    private List<Integer> bpDataList = new ArrayList<>();
    private int currentCount = 0;
    private long beforeErrorCount = 0;
    private Timer timerResendData;
    public static boolean iserror = false;
    private int stateCount = 0;
    private int i = 0;
    public static int DROPMEASURE = -1;
    public Object obStr = "0";
    public int mesurebp, endsys, enddia, endhr;
    public int battey, user_mode;
    public String blestate;
    public String ver;
    public String time, devstate;

    public boolean guest;

    public ArrayList<SycnBp> bps = new ArrayList<>();
    public OnWBPDataListener onWBPDataListener;

    public interface OnWBPDataListener {
        void onMeasurementBp(int temp);

        void onMeasurementfin(int SYS, int DIA, int HR, Boolean isguestmode);

        void onErroer(Object obj);

        void onState(int btbattey, String bleState, String version, String devState);

        void onSycnBp(ArrayList<SycnBp> sycnBps);

        void onTime(String wbp_time);

        void onUser(int user);
    }


    @Override
    public void setOnWBPDataListener(OnWBPDataListener listener) {

        onWBPDataListener = listener;
    }

    @Override
    public synchronized void resolveBPData2(byte[] datas, final BluetoothLeClass mBLE, Activity activity) {
        try {
            for (int i = 0; i < datas.length; i++) {
                if (datas[i] < 0)
                    bpDataList.add(datas[i] + 256);
                else
                    bpDataList.add((int) datas[i]);
            }
            int length = bpDataList.size();
            String tstr = "";
            for (int j = 0; j < length; j++) {
                tstr = tstr + "," + bpDataList.get(j) + "";
                if (bpDataList.get(j) == 170 && j < length - 1 && bpDataList.get(j + 1) != 170) {
                    int n = bpDataList.get(j + 1);
                    int sum = n;
                    if (j + 1 + n > length) {

                        break;
                    }
                    for (int k = 0; k < n - 1; k++) {
                        if (j + 1 + n > length) {
                            break;
                        }
                        if (bpDataList.get(j + 1 + k + 1) == 170) {
                            if (bpDataList.get(j + 1 + k + 2) == 170) {
                                k++;
                                n++;
                            } else {
                                Log.d("test", "exception,the data have no 0xAA");
                                return;
                            }
                        }
                        int add = bpDataList.get(j + 1 + k + 1);

                        Log.d("muk", "received add��������������������" + add);
                        Log.d("muk", "received sum��������������������" + sum);
                        sum = sum + add;
                        Log.d("muk", "received sum2��������������������" + sum);
                    }
                    if (j + 1 + n > length) {
                        return;

                    }
                    List<Integer> bytesTwo = new ArrayList<Integer>();
                    for (int m = 0; m < n - 1; m++) {

                        bytesTwo.add(bpDataList.get(j + 1 + m + 1));
                        if (bpDataList.get(j + 1 + m + 1) == 170 && bpDataList.get(j + 1 + m + 2) == 170) {
                            m++;
                        }
                    }
                    int pID = bytesTwo.get(0);
                    if (pID == 43) {
                        Log.d("nice",
                                "--------------------now is ,synchronization----------------------------------------------------");
                        if (SettingUtil.resendData == true) {
                            Log.d("nice",
                                    "--------------------synchronization-----resenddata-----------------------------------------------");
                            SettingUtil.isHaveData = true;
                        } else {
                            currentCount++;
                        }

                    }
                    // ��ȡУ����ֽ�
                    int checksum = bpDataList.get(j + 1 + n); // ��ʱ��nΪ�����˶���0xAA��ʵ�ʳ���
                    // У��
                    if (checksum != sum % 256 && checksum + sum % 256 != 256) {
                        // У��ʧ�ܣ������쳣
                        Log.d("muk", "check failed��data exception��������������������");
                        if (pID == 43) {
                            Log.d("nice",
                                    "--------------------now is ,synchronization--------------------------------------------------");
                            if (SettingUtil.resendData) {// �ط���û�гɹ��ġ��ͷ��ͽ�������
                                // ������������
                                finishToResendData(mBLE);
                                return;
                            } else {

                                Log.d("nice",
                                        "--------------------now is ,synchronization..add-----------------------------------------------");
                                beforeErrorCount++;
                                BleData mydata = new BleData();
                                mydata.setByte1(bytesTwo.get(1));
                                mydata.setByte2(bytesTwo.get(2));
                                mydata.setByte3(bytesTwo.get(3));
                                mydata.setByte4(bytesTwo.get(4));
                                SampleGattAttributes.listdata.add(mydata);
                            }

                        }
                        continue;
                    }

                    Log.d("muk", "received pid��������������������" + pID);
                    Log.d("muk", bytesTwo.size()
                            + "-<------------------------first-----pID===============����" + pID);
                    switch (pID) {
                        case 40:
                            String byte3 = byteToBit(bytesTwo.get(3));
                            Log.d("muk", "length-----------------" + bytesTwo.size());
                            String mode = byte3.charAt(6) + "" + byte3.charAt(7);
                            if (mode.equals("01")) {// ������ʹ���
                                Log.d("mye", "is 01");
                                return;
                            } else if (mode.equals("00")) {
                                byte tempCuff = (byte) (BitToByte(byte3.charAt(0) + "") + bytesTwo.get(1));

                                int tempL = tempCuff;
                                if (tempL < 0) {
                                    tempL += 256;
                                }
                                Log.d("mye", "�� 00");
                                Log.d("mye", "Cuff pressure������-----------------------=" + tempL);
                                mesurebp = tempL;
                            }

                            SettingUtil.isSyncFinish = true;
                            break;
                        case 41: // �������
                            Log.d("muk", "PID is-----------------41");
                            String byteResult = byteToBit(bytesTwo.get(1));
                            SettingUtil.isNowTestBpFinish = true;
                            Log.d("muk", "the test result--------------------------byteResult------����" + byteResult);
                            String tempR = byteResult.charAt(3) + "";
                            Log.d("muk", "the test result--------------------------tempR------����" + tempR);
                            if (tempR.equals("0")) {
                                Log.d("muk", "the test result-----------------normal---------------");
                                iserror = false;
                                int SYS = bytesTwo.get(3);
                                if (SYS < 0) {
                                    SYS += 256;
                                }
                                int dp = bytesTwo.get(4);
                                int hr = bytesTwo.get(6);
                                if (dp < 0) {
                                    dp += 256;
                                }
                                if (hr < 0) {
                                    hr += 256;
                                }
//
                                endsys = SYS;
                                enddia = dp;
                                endhr = hr;
                                guest = isGusetmode;
                                int three = bytesTwo.get(7) < 0 ? bytesTwo.get(7) + 256
                                        : bytesTwo.get(7);
                                int four = bytesTwo.get(8) < 0 ? bytesTwo.get(8) + 256
                                        : bytesTwo.get(8);
                                int five = bytesTwo.get(9) < 0 ? bytesTwo.get(9) + 256
                                        : bytesTwo.get(9);
                                int six = bytesTwo.get(10) < 0 ? bytesTwo.get(10) + 256
                                        : bytesTwo.get(10);
                                /**
                                 * ���ﷵ��һ����ǰʱ��
                                 */
                                int[] times = {three, four, five, six};
                                time = BuleToTime(times);
                                Log.d("testing time", "" + time);


                                Log.d("muk", "the result is ----------------SYSlow 8 bit=" + SYS);
                                Log.d("muk", "the result is -----------------DIA=" + dp);
                                Log.d("muk", "the result is -----------------PR=" + hr);
//                            Log.d("muk", "���������---------MainActivity--------=" + MainActivity.isGusetmode);
                                Log.d("muk", "the result is ------------SettingUtil-----=" + isGusetmode);
                                //��ѹ...��ѹ ...����
                                if (SYS == 0 || dp == 0 || hr == 0) {
                                    if (SYS == 0) {
                                        obStr = activity.getApplication().getString(R.string.ble_test_error15);
                                    } else if (dp == 0) {
                                        obStr = activity.getApplication().getString(R.string.ble_test_error16);
                                    } else if (hr == 0) {
                                        obStr = activity.getApplication().getString(R.string.ble_test_error17);
                                    }
                                    iserror = true;
                                } else {
                                    obStr = "0";
                                }

                            } else {
                                Log.d("muk",
                                        "test result is -----------------wrong----bytesTwo.get(2)-----------------------"
                                                + bytesTwo.get(2));
                                iserror = true;
                                /**
                                 * �����б�
                                 */
                                obStr = getErrorTip(bytesTwo, activity);
//
                            }
                            Log.d("mgf", "----------------bytesTwo------------------------" + bytesTwo.toString());

                            break;

                        case 42:// Ѫѹ��״̬(ID:42)
                            Log.d("muk", "PID��-------������the wbp state��������������������������������������������----------42");

                            int BTBatteryCopy = bytesTwo.get(1);
                            Log.d("WT", ".BTBatteryCopy...=" + BTBatteryCopy);
                            if (BTBatteryCopy < 0) {
                                BTBatteryCopy = BTBatteryCopy + 256;
                                SettingUtil.BTBattery = (bytesTwo.get(1) + 256) % 16;
                            } else {
                                SettingUtil.BTBattery = bytesTwo.get(1) % 16;
                            }
                            Log.d("muk", "BTBattery:::" + SettingUtil.BTBattery);
                            String tempB = setValue(Integer.toBinaryString(BTBatteryCopy));
                            Log.d("muk", "byte(1):::" + tempB);
                            String temp1 = tempB.charAt(3) + "";
                            SettingUtil.bleState = temp1;
                            String temp2 = tempB.charAt(5) + "" + tempB.charAt(6) + "" + tempB.charAt(7) + "";
                            String temp3 = tempB.charAt(2) + "" + tempB.charAt(1) + "" + tempB.charAt(0);
                            Log.d("lgc", "the state of the device:::" + temp1);
                            Log.d("lgc", "the work mode of the device:::" + Integer.valueOf(temp2, 2));
                            if (temp1.equals("1")) {
                                SettingUtil.isSyncFinish = false;
                            } else {
                                SettingUtil.isSyncFinish = true;
                                iserror = false;
                            }
                            Log.d("lgc", "isSyncFinish:::" + SettingUtil.isSyncFinish);
                            int version = bytesTwo.get(2);
                            SettingUtil.tempVersion = Integer.toString((version / 16)) + "."
                                    + Integer.toString((version % 16));
                            int three = bytesTwo.get(4) < 0 ? bytesTwo.get(4) + 256
                                    : bytesTwo.get(4);
                            int four = bytesTwo.get(5) < 0 ? bytesTwo.get(5) + 256
                                    : bytesTwo.get(5);
                            int five = bytesTwo.get(6) < 0 ? bytesTwo.get(6) + 256
                                    : bytesTwo.get(6);
                            int six = bytesTwo.get(7) < 0 ? bytesTwo.get(7) + 256
                                    : bytesTwo.get(7);

                            int[] times = {three, four, five, six};
                            time = BuleToTime(times);

                            Log.d("WT", "BTBattery....." + SettingUtil.BTBattery);
                            Log.d("WT", "SettingUtil.tempVersion....." + SettingUtil.tempVersion);

                            byte[] t_data4 = {(byte) 0xAA, 0x04, 0x03, 42, 0x00, 0x00};// ��Ӧ
                            mBLE.writeCharacteristic_wbp(t_data4);
                            if (SettingUtil.isfirstLoad5 == 0) {
                                Log.d("lgc", "..1...");
                                Timer timer = new Timer();
                                timer.schedule(new TimerTask() {

                                    @Override
                                    public void run() {
                                        getNowDateTime(mBLE);
                                    }
                                }, 800);
                            }
                            SettingUtil.isfirstLoad5++;
                            devstate = temp3;
                            battey = SettingUtil.BTBattery;
                            blestate = temp1;
                            ver = SettingUtil.tempVersion;


                            break;
                        case 43:// Ѫѹͬ�����ݰ�(ID:43)
                            Log.d("muk", "PID��-------sync��������������������������������������������������----------43");
                            Log.d("muk", stateCount
                                    + "+++++stateCount------------------------currentCount++++"
                                    + currentCount);

                            String byteData = byteToBit(bytesTwo.get(5));

                            String tempError = byteData.charAt(3) + "";
                            Log.d("muk", "sync--------------------------tempError------����" + tempError);
                            Log.d("muk", "bytesTwo.get(5)--------------" + bytesTwo.get(5));
                            Log.d("muk", "great---------------");
                            iserror = false;
                            int SYS = bytesTwo.get(6);//

                            int dp = bytesTwo.get(7);//

                            int hr = bytesTwo.get(9);//
                            if (SYS < 0) {
                                SYS += 256;
                            }
                            if (dp < 0) {
                                dp += 256;
                            }
                            if (hr < 0) {
                                hr += 256;
                            }

                            String MeasureTime = getBleTestTime(bytesTwo.get(10), bytesTwo.get(11),
                                    bytesTwo.get(12), bytesTwo.get(13));
                            SycnBp sycnBp = new SycnBp();
                            sycnBp.setDia(dp);
                            sycnBp.setSys(SYS);
                            sycnBp.setHr(hr);
                            sycnBp.setTime(MeasureTime);

                            Log.d("muk", "sync������ the result of SYS-----------------SYSlow8=" + SYS);
                            Log.d("muk", "sync������ -----------------DIA=" + dp);
                            Log.d("muk", "sync������ the result of-----------------PR=" + hr);
                            Log.d("muk", "sync������ the result of-----------------MeasureTime=" + MeasureTime);

                            bps.add(sycnBp);
                            String str1 = byteData.charAt(1) + "";
                            String str2 = byteData.charAt(2) + "";
                            String str3 = str1 + str2;
                            // 0x01���û�A��
                            // 0x02���û�B��
                            // 0x03���ο�ģʽ��
                            int user = Integer.valueOf(str3, 2);
                            Log.d("muk", ",,,,,,,,,,,,,,,,,,,,,,user," + user);
                            int FamilyId = -1;

                            if (SYS != 0 && dp != 0 && hr != 0) {

                                if (isGusetmode) {
                                    SettingUtil.Sbp = SYS;
                                    SettingUtil.Dbp = bytesTwo.get(7);
                                    SettingUtil.Hr = bytesTwo.get(9);
                                } else {
                                    if (user == 1 || user == 2) {

                                    } else {
                                        SettingUtil.Sbp = SYS;
                                        SettingUtil.Dbp = dp;
                                        SettingUtil.Hr = hr;
                                        SettingUtil.guestBpTime = MeasureTime;
                                    }
                                }
                            }
                            if (stateCount == currentCount) {// �������
                                Log.d("muk",
                                        "--------------------------------------finish sync-------nice------------------------------------beforeErrorCount++++"
                                                + beforeErrorCount);
                                if (beforeErrorCount != 0) {// ���ڴ����ط���ʷ����
                                    Log.d("muk",
                                            "--------------------------------------ͬ��-���ڴ����ط���ʷ����--------����-------------------------------------");
                                    if (SettingUtil.resendData) {// ������ɹ��ط�����������
                                        if (SampleGattAttributes.listdata.size() > 0) {
                                            SampleGattAttributes.listdata.remove(0);
                                        }
                                        if (SampleGattAttributes.listdata.size() > 0) {
                                            Log.d("muk",
                                                    "--------------------------------------ͬ��-����������ʷ����--------------����--------------------------------");
                                            reSendData(mBLE);
                                        } else {// û�д���,���ͽ���
                                            syncFinish(mBLE);
                                        }
                                    } else {// �״��ط�
                                        reSendData(mBLE);
                                    }
                                } else {// û�д���,���ͽ���

                                    syncFinish(mBLE);
                                }
                            }
                            // }
                            break;
                        case 44:// Ѫѹͬ�����ݿ�ʼ״̬��(ID:44)

                            Log.d("muk", "PID��------Ѫѹͬ�����ݿ�ʼ״̬��-----------44");
                            Log.d("muk", "PID��-----------	1------44=====" + bytesTwo.get(1));// �����ݿ���
                            // bit0~7
                            Log.d("muk", "PID��----------2-------44=====" + bytesTwo.get(2));// �����ݿ���
                            // Bit8~15
                            Log.d("muk", "PID��-----------	3------44=====" + bytesTwo.get(3));
                            Log.d("muk", "PID��----------4-------44=====" + bytesTwo.get(4));
                            int a1 = bytesTwo.get(3);
                            int a2 = bytesTwo.get(4);
                            stateCount = a1 + a2;
                            Log.d("stateCount", "����ʲô.......����" + stateCount);
                            String t1 = setValue(Integer.toBinaryString(a1));
                            String t2 = setValue(Integer.toBinaryString(a2));
                            Log.d("muk", "PID��--------------t1--������=====" + t1);
                            Log.d("muk", "PID��---------------t2-������=====" + t2);
                            Log.d("muk", "PID��---------------stateCount-������== ���ݰ�������Ϊ===" + stateCount);
                            byte[] t_data2 = {(byte) 0xAA, 0x04, 0x03, 44, 0x00, 0x00};
                            mBLE.writeCharacteristic_wbp(t_data2);
                            break;
                        case 45:// Ѫѹ�洢״̬��(ID:45)

                            Log.d("muk", "PID��--------Ѫѹ�洢״̬��---------45");
                            Log.d("muk", "PID��-----------	1------45=====" + bytesTwo.get(1));// ���ݿ�����1
                            // bit0~7
                            Log.d("muk", "PID��----------2-------45=====" + bytesTwo.get(2));// ���ݿ�����0
                            // Bit8~15
                            /*
                             * �ж����ݿ��Ƿ����0
                             */
                            int Data_blockCount = bytesTwo.get(1) + bytesTwo.get(2);
                            Log.d("muk", "Data_blockCount---------::" + Data_blockCount);
                            Log.d("muk", "���ݿ�---------::" + stateCount);
                            if (Data_blockCount > 0) {
                                if (SettingUtil.bleState.equals("0")) {
                                    if (SettingUtil.isfirstLoad6 == 0) {
                                        Timer timer = new Timer();
                                        timer.schedule(new TimerTask() {

                                            @Override
                                            public void run() {
                                                Log.d("muk", "=====��--------����ʼ������ʷ����");

                                                mBLE.writeCharacteristic_wbp(SampleGattAttributes.data4); // ����ʼ������ʷ����(ID:122)
                                            }
                                        }, 600);
                                    }
                                    SettingUtil.isfirstLoad6++;
                                    mBLE.writeCharacteristic_wbp(SampleGattAttributes.data4);
                                }

                            } else {
                                SettingUtil.isSyncFinish = true;
                                // if (MainActivity.Position == 0) {
                                Log.d("muk", "=====Ѫѹ�洢״̬��(ID:45)--------stateCount++++" + stateCount);
                                if (stateCount != 0) {
                                    if (SettingUtil.isfirstLoad10 == 0) {
                                        Log.d("muk", "=====Ѫѹ�洢״̬��(ID:45)--------ͬ�����++++-----------");
                                    }
                                    SettingUtil.isfirstLoad10++;

                                } else {
                                    Log.d("muk", "=====Ѫѹ�洢״̬��(ID:45)--------û�����ݰ�++++----------");
                                }
                            }

                            break;
                        case 3:// ��Ӧ��
                            int tempid = bytesTwo.get(1);
                            if (tempid < 0) {
                                tempid = tempid + 256;
                            }
                            Log.d("muk", "PID��-----------�����·�������	------46=====����" + tempid);
                            Log.d("muk", "-----------------���ص�Ӧ����====����" + bytesTwo.get(2));
                            if (tempid == 128) {
                                Log.d("�û��ı�", tempid + "");
                                if (!isGusetmode) {
                                    if (WBPMODE == 0) {
                                        Timer timer = new Timer();
                                        timer.schedule(new TimerTask() {

                                            @Override
                                            public void run() {
                                                getNowDateTime(mBLE);
                                            }
                                        }, 600);
                                    }
                                }

                            }
                            if (tempid == 107) {
                                Log.d("muk", "SettingUtil.isfirstLoad7-------" + SettingUtil.isfirstLoad7);
                                if (SettingUtil.isfirstLoad7 == 0) {
                                    if (!isGusetmode && SettingUtil.bleState.equals("0")) {
                                    } else {
                                        // ����ģʽ
                                        Log.d("muk", "syncDataByBle-------����ģʽ��    �·��û�-------");
                                        byte[] data = {(byte) 0xAA, 0x04, (byte) 0x80, 0x03, 0x00, 0x00};
                                        mBLE.writeCharacteristic_wbp(data);
                                    }
                                }
                                SettingUtil.isfirstLoad7++;

                            }
                            if (bytesTwo.get(2) == 85) {
                                Log.d("muk", "----------Ӧ������=====" + DROPMEASURE);
                                if (bytesTwo.get(1) == 120 && DROPMEASURE == 8) {
                                    iserror = true;
                                }
                            } else if (bytesTwo.get(2) == 0) {
                                Log.d("muk", "----------������=====");
                            } else {
                                Log.d("muk", "----------��������=====");
                            }

                            Log.d("muk", "-----------------end====----------------");
                            break;
                        case 49:
                            Log.d("muk", "PID��-----------------49");
                            Log.d("muk", "WBPMODE-----------------::" + WBPMODE);
                            Log.d("muk", "����ģʽ----------------::" + isGusetmode);
                            if (!isGusetmode) {
                                if (!SettingUtil.isTest || SettingUtil.isFirstopenBle) {
                                    SettingUtil.isFirstopenBle = false;
                                    user_mode = bytesTwo.get(1);
                                    Log.d("user_mode::", "" + user_mode);
                                    byte[] t_data3 = {(byte) 0xAA, 0x04, 0x03, 49, 0x00, 0x00};
                                    mBLE.writeCharacteristic_wbp(t_data3);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            Log.d("shani", "����������data��������������������" + tstr);
            bpDataList.clear();
            onWBPDataListener.onMeasurementBp(mesurebp);
            onWBPDataListener.onMeasurementfin(endsys, enddia, endhr, guest);
            onWBPDataListener.onErroer(obStr);
            onWBPDataListener.onState(battey, blestate, ver, devstate);
            onWBPDataListener.onSycnBp(bps);
            onWBPDataListener.onTime(time);
            onWBPDataListener.onUser(user_mode);


        } catch (Exception e) {
            e.printStackTrace();
            bpDataList.clear();
            e.printStackTrace();
        }
    }


    public void SendForAll(BluetoothLeClass mBLE) {
        mBLE.writeCharacteristic_wbp(SampleGattAttributes.data7);
    }

    public static String BuleToTime(int[] times) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (int i = 0; i < times.length; i++) {
            times[i] = times[i] > 0 ? times[i] : (times[i] + 256);
        }
        int time = times[3] * 256 * 256 * 256 + times[2] * 256 * 256 + times[1]
                * 256 + times[0];
        Long c = null;
        try {
            c = sf.parse("2000-01-01 00:00:00").getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Date d = new Date((long) time * (long) 1000 + c);
        return sf.format(d);
    }

    public static byte BitToByte(String byteStr) {
        int re, len;
        if (null == byteStr) {
            return 0;
        }
        len = byteStr.length();
        if (len != 4 && len != 8) {
            return 0;
        }
        if (len == 8) {// 8 bit����
            if (byteStr.charAt(0) == '0') {// ����
                re = Integer.parseInt(byteStr, 2);
            } else {// ����
                re = Integer.parseInt(byteStr, 2) - 256;
            }
        } else {// 4 bit����
            re = Integer.parseInt(byteStr, 2);
        }
        return (byte) re;
    }


    public static String byteToBit(int b) {
        return "" + (byte) ((b >> 7) & 0x1) + (byte) ((b >> 6) & 0x1) + (byte) ((b >> 5) & 0x1)
                + (byte) ((b >> 4) & 0x1) + (byte) ((b >> 3) & 0x1) + (byte) ((b >> 2) & 0x1)
                + (byte) ((b >> 1) & 0x1) + (byte) ((b >> 0) & 0x1);
    }

    private String setValue(String data) {
        int len = data.length();
        String tempDate = "";
        Log.d("muk", len + "data...." + data);
        switch (len) {
            case 1:
                tempDate = "0000000" + data;
                break;
            case 2:
                tempDate = "000000" + data;
                break;
            case 3:
                tempDate = "00000" + data;
                break;

            case 4:
                tempDate = "0000" + data;
                break;

            case 5:
                tempDate = "000" + data;
                break;

            case 6:
                tempDate = "00" + data;
                break;
            case 7:
                tempDate = "0" + data;
                break;
            default:
                tempDate = data;
                break;
        }
        return tempDate;
    }


    private void finishToResendData(BluetoothLeClass mBLE) {
        SettingUtil.resendData = false;
        SettingUtil.isHaveData = false;
        currentCount = 0;
        beforeErrorCount = 0;
        if (timerResendData != null) {
            timerResendData.cancel();
            timerResendData = null;
        }
        Log.d("muk",
                "--------------------������ͬ��---�����ط�---�����ˡ�-------------------------------------------------");
        byte[] data6 = {(byte) 0xAA, 0x04, (byte) 0x7F, 0x01, 0x00, 0x00};
        mBLE.writeCharacteristic_wbp(data6);
    }

    /**
     * ��ȡ�豸������ʱ��
     *
     * @param b1
     * @param b2
     * @param b3
     * @param b4
     * @return
     */
    private String getBleTestTime(int b1, int b2, int b3, int b4) {
        String strNowTime = "";
        try {
            int a1 = b1;
            int a2 = b2;
            int a3 = b3;
            int a4 = b4;
            if (a1 < 0) {
                a1 += 256;
            }
            if (a2 < 0) {
                a2 += 256;
            }
            if (a3 < 0) {
                a3 += 256;
            }
            if (a4 < 0) {
                a4 += 256;
            }
            String t1 = setValue(Integer.toBinaryString(a1));
            String t2 = setValue(Integer.toBinaryString(a2));
            String t3 = setValue(Integer.toBinaryString(a3));
            String t4 = setValue(Integer.toBinaryString(a4));
            String temp4 = t4 + t3 + t2 + t1;
            BigInteger src1 = new BigInteger(temp4, 2);// ת��ΪBigInteger����
            String millSecond = src1.toString();// ת��Ϊ10���Ʋ�������
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = df.parse("2000-01-01 00:00:00");
            long datet = date.getTime();
            long time = Long.parseLong(millSecond);
            long tempt = time * 1000;
            long nowTime = tempt + datet;
            Date d = new Date(nowTime);
            strNowTime = df.format(d).toString();
            Log.d("muk", "now time������" + df.format(d).toString());
        } catch (Exception e) {
            e.printStackTrace();
            strNowTime = getDateFormatToString(null);
        }

        return strNowTime;
    }


    public static String getDateFormatToString(String format) {
        Date date = new Date();
        if (format == null) {
            format = "yyyy-MM-dd HH:mm:ss.SSS";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        String dateStr = dateFormat.format(date);
        return dateStr;
    }

    /**
     * �·���ǰ��ʱ�䵽�豸У��
     */

    public void getNowDateTime(BluetoothLeClass mBLE) {
        Log.d("lgc", "�·�ʱ��2....");
        try {
            String nowdata = getDateFormatToString(null);// ��ǰʱ��
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date now = df.parse(nowdata);
            Date date = df.parse("2000-01-01 00:00:00");
            long l = now.getTime() - date.getTime();
            long day = l / (24 * 60 * 60 * 1000);
            long hour = (l / (60 * 60 * 1000) - day * 24);
            long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
            long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
            int alltime = (int) (day * 24 * 60 * 60 + hour * 60 * 60 + min * 60 + s);
            Log.d("muk", "..��ǰ��ʱ�� ��alltime��..+" + alltime);
            String str = print(alltime);
            String str1 = "";
            String str2 = "";
            String str3 = "";
            String str4 = "";
            for (int i = 0; i < str.length(); i++) {
                if (i < 8) {
                    str1 += str.charAt(i) + "";
                } else if (i < 16 && i >= 8) {
                    str2 += str.charAt(i) + "";
                } else if (i < 24 && i >= 16) {
                    str3 += str.charAt(i) + "";
                } else {
                    str4 += str.charAt(i) + "";
                }
            }
            int a = Integer.valueOf(str4, 2);
            final byte temp4 = (byte) a;
            int b = Integer.valueOf(str3, 2);
            final byte temp3 = (byte) b;
            int c = Integer.valueOf(str2, 2);
            final byte temp2 = (byte) c;
            int d = Integer.valueOf(str1, 2);
            final byte temp1 = (byte) d;
            Log.d("muk", "..��ǰ��ʱ�� ��tempB4��..+" + temp4 + "-" + temp3 + "-" + temp2 + "-" + temp1);
            byte[] data6 = {(byte) 0xAA, 0x08, (byte) 0x6B, 0x00, temp4, temp3, temp2, temp1, 0x00, 0x00};
            mBLE.writeCharacteristic_wbp(data6);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * �ط���ʷ��¼
     */
    private void reSendData(final BluetoothLeClass mBLE) {
        i = 0;
        /**
         * �ط���ʷ��¼
         */
        timerResendData = new Timer();
        timerResendData.schedule(new TimerTask() {

            @Override
            public void run() {
                if (i == 3 && !SettingUtil.isHaveData) {
                    finishToResendData(mBLE);
                    SettingUtil.isHaveData = false;
                    timerResendData.cancel();
                    timerResendData = null;
                } else {
                    SettingUtil.resendData = true;
                    mBLE.writeCharacteristic_wbp(SampleGattAttributes.resendBleData(
                            SampleGattAttributes.listdata.get(0).getByte1(), SampleGattAttributes.listdata
                                    .get(0).getByte2(), SampleGattAttributes.listdata.get(0).getByte3(),
                            SampleGattAttributes.listdata.get(0).getByte4()));
                }
                i++;

            }
        }, 0, 1000); // ��ʱ0ms��ִ�У�1000msִ��һ��
    }


    /**
     * ������ ������������ 127 +00
     */
    private void syncFinish(final BluetoothLeClass mBLE) {
        Log.d("muk", "--------------------------------------ͬ��-û�д���,���ͽ���-------------nice---------------------------------");
        SettingUtil.resendData = false;
        SettingUtil.isHaveData = false;
        currentCount = 0;
        beforeErrorCount = 0;
        // ������������
        mBLE.writeCharacteristic_wbp(SampleGattAttributes.data6);
        /**
         * �ٴβ�ѯѪѹ״̬�Ƿ� �������ݰ�
         */
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                Log.d("muk", "syncDataByBle-------ͬ�����ݿ�ʼ-��ѯѪѹ��״̬-------");
                mBLE.writeCharacteristic_wbp(SampleGattAttributes.data7);
            }
        }, 1000);
    }


    private String print(int i) {
        String str = "";
        for (int j = 31; j >= 0; j--) {
            if (((1 << j) & i) != 0) {
                str += "1";
            } else {
                str += "0";
            }
        }
        return str;

    }

    /**
     * ������Ϣ��ʾ��
     *
     * @param bytesTwo
     * @return
     */
    private Object getErrorTip(List<Integer> bytesTwo, Activity activity) {
        Object obStr = "0";
        if (bytesTwo.get(2) == 7 || bytesTwo.get(2) == 14) {
            obStr = activity.getApplication().getString(R.string.ble_test_error3);
        } else if (bytesTwo.get(2) == 6 || bytesTwo.get(2) == 20) {
            obStr = activity.getApplication().getString(R.string.ble_test_error2);
        } else if (bytesTwo.get(2) == 2 || bytesTwo.get(2) == 8 || bytesTwo.get(2) == 10 || bytesTwo.get(2) == 12 || bytesTwo.get(2) == 15) {
            obStr = activity.getApplication().getString(R.string.ble_test_error6);
        } else if (bytesTwo.get(2) == 11 || bytesTwo.get(2) == 13) {
            obStr = activity.getApplication().getString(R.string.ble_test_error7);
        } else if (bytesTwo.get(2) == 9 || bytesTwo.get(2) == 19) {
            obStr = activity.getApplication().getString(R.string.ble_test_error5);
        } else {
            obStr = activity.getApplication().getString(R.string.ble_test_error14);
        }
        return obStr;
    }

    public void onSingleCommand(BluetoothLeClass mBLE) {
        mBLE.writeCharacteristic_wbp(SampleGattAttributes.data);
    }

    @Override
    public void onStopBleCommand(BluetoothLeClass mBLE) {
        mBLE.writeCharacteristic_wbp(SampleGattAttributes.data2);
    }

    @Override
    public void sendUserInfoToBle(BluetoothLeClass mBLE) {
        if (mBLE != null) {
            if (SettingUtil.userModeSelect == 1) {
                byte[] data = {(byte) 0xAA, 0x04, (byte) 0x80, 0x01, 0x00, 0x00};
                mBLE.writeCharacteristic_wbp(data);
            } else if (SettingUtil.userModeSelect == 2) {
                byte[] data = {(byte) 0xAA, 0x04, (byte) 0x80, 0x02, 0x00, 0x00};
                mBLE.writeCharacteristic_wbp(data);
            }
        }
    }
}
