package com.example.bluetoothlibrary.Interface

import android.app.Activity
import com.example.bluetoothlibrary.BluetoothLeClass
import com.example.bluetoothlibrary.Impl.ResolveWbp.OnWBPDataListener

/**
 * Created by laiyiwen on 2017/4/28.
 */
interface Wbpdata {
    fun resolveBPData2(datas: ByteArray?, mBLE: BluetoothLeClass?, activity: Activity?)
    fun setOnWBPDataListener(listener: OnWBPDataListener?)
    fun getNowDateTime(mBLE: BluetoothLeClass?)
    fun SendForAll(mBLE: BluetoothLeClass?)
    fun onSingleCommand(mBLE: BluetoothLeClass?)
    fun onStopBleCommand(mBLE: BluetoothLeClass?)
    fun sendUserInfoToBle(mBLE: BluetoothLeClass?)
}