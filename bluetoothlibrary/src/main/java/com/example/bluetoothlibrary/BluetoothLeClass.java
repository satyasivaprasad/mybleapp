/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.bluetoothlibrary;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.util.Log;

import com.example.bluetoothlibrary.entity.ConnectBleServiceInfo;
import com.example.bluetoothlibrary.entity.Constant;
import com.example.bluetoothlibrary.entity.Peripheral;
import com.example.bluetoothlibrary.entity.SampleGattAttributes;

import java.util.List;
import java.util.UUID;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public class BluetoothLeClass {
    private final static String TAG = BluetoothLeClass.class.getSimpleName();
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    public BluetoothGattCallback mGattCallback;
    private boolean mScanning = true;
    protected boolean isOnServiceConnected = false;
    public static String connectingDevice;//the connecting device
    protected BluetoothAdapter.LeScanCallback mLeScanCallback;

    // UUID
    public final static String kReadUUID = "0000ffe1-0000-1000-8000-00805f9b34fb";
    public final static String kServiceUUID = "0000ffe0-0000-1000-8000-00805f9b34fb";


    public interface OnDataAvailableListener {
        void onCharacteristicRead(BluetoothGatt gatt,
                                  BluetoothGattCharacteristic characteristic);

        void onCharacteristicWrite(BluetoothGatt gatt,
                                   BluetoothGattCharacteristic characteristic);

    }

    public interface OnsetDevicePreipheral {
        void setDevicePreipheral(BluetoothDevice device, int model, String SN, float protocolVer);
    }

    private OnsetDevicePreipheral onsetDevicePreipheral;
    private OnDataAvailableListener mOnDataAvailableListener;
    private Context mContext;

    public void setOnDataAvailableListener(OnDataAvailableListener l) {
        mOnDataAvailableListener = l;
    }

    public void setOnsetDevicePreipheral(OnsetDevicePreipheral l) {
        onsetDevicePreipheral = l;
    }

    public BluetoothLeClass(Context c) {
        mContext = c;
    }

    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    public void setLeScanCallback() {
        mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(final BluetoothDevice device, int rssi, final byte[] scanRecord) {
                Log.e("device", device.toString());

                if (device.getName() != null) {
                    switch (device.getName().trim()) {
                        case "BLT_WBP":
                            StringBuilder SN_bp = new StringBuilder();
                            for (int i = 13; i < scanRecord.length; i++) {
                                if (scanRecord[i] == 0)
                                    break;
                                SN_bp.append((char) scanRecord[i]);
                            }
                            onsetDevicePreipheral.setDevicePreipheral(device, scanRecord[12], SN_bp.toString(), scanRecord[10]);
                            break;
                        case "AL_WBP":
                            String hexString_al = bytesToHexString(scanRecord);
                            if (hexString_al != null && hexString_al.length() > (64 + 32)) {
                                String hexStr_al = hexString_al.substring(64, 64 + 32);
                                Peripheral peripheral = resolveBleMsg_bp(hexStr_al);
                                onsetDevicePreipheral.setDevicePreipheral(device, Integer.parseInt(peripheral.getModel()), peripheral.getPreipheralSN(), peripheral.getProtocolVer());
                            }
                        default:
                            break;
                    }
                }
            }
        };

    }

    public static String bytesToHexString(byte[] src){
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (byte b : src) {
            int v = b & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    public static Peripheral resolveBleMsg_bp(String hexStr) {
        int lenth = 2;
        int i = 4;
        String currentResult = "";
        while (i * lenth < hexStr.length()) {
            String targetStr = "";
            if (i * lenth + lenth > hexStr.length()) {
                targetStr = hexStr.substring(i * lenth);
            } else {
                targetStr = hexStr.substring(i * lenth, i * lenth + lenth);
            }

            long hexValue = strtoul(targetStr, 16);
            int value = Integer.parseInt(String.valueOf(hexValue));
            if ((char) value == 0) {
                break;
            }
            currentResult = currentResult.concat((char) value + "");
            i++;
        }
        long protocolVersion = strtoul(hexStr.substring(2, 4), 16);
        long deviceModel = strtoul(hexStr.substring(6, 8), 16);

        Peripheral peripheral = new Peripheral();
        peripheral.setModel(String.valueOf(deviceModel));
        peripheral.setPreipheralSN(currentResult);
        peripheral.setProtocolVer(protocolVersion);
        return peripheral;
    }

    /*
     * blurtooth scan
     */
    public void scanLeDevice(final boolean enable) {
        if (mBluetoothAdapter != null) {
            if (enable) {
                mBluetoothAdapter.startLeScan(mLeScanCallback);// BTState = 3;
                mScanning = true;
            } else {
                if (mLeScanCallback != null)
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                mScanning = false;
            }
        }
    }

    public void setBluetoothGattCallback() {
        mGattCallback = new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                String intentAction;
                Log.d(TAG, "onConnectionStateChange:-change--");
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    Log.d(TAG, "Attempting to start service discovery:" + mBluetoothGatt.discoverServices());
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    Log.d(TAG, "Disconnected from GATT server.");
                }
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                System.out.println("onServicesDiscovered..............=============");
                Log.d(TAG, "onServicesDiscovered:---");
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    ConnectBleServiceInfo connectServiceInfo = new ConnectBleServiceInfo();
                    connectServiceInfo.setDeviceName(connectingDevice);
                    connectServiceInfo.setServiceUUID(SampleGattAttributes.SeviceIDfbb0);
                    connectServiceInfo.setCharateUUID(SampleGattAttributes.GetCharacteristicIDfbb2);
                    connectServiceInfo.setCharateReadUUID(SampleGattAttributes.GetCharacteristicIDfbb1);
                    connectServiceInfo.setConectModel(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    displayGattServices(getSupportedGattServices(), connectServiceInfo);
                } else {
                    Log.d(TAG, "onServicesDiscovered exception hanppen: " + status);
                }
            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                mOnDataAvailableListener.onCharacteristicRead(gatt, characteristic);
            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                System.out.println("onCharacteristicRead..............==========");
                Log.d("test", "onCharacteristicRead---");
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    Log.d("test", "BluetoothGatt.GATT_SUCCESS");
                }
            }

            @Override
            public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic,
                                              int status) {
                super.onCharacteristicWrite(gatt, characteristic, status);
                System.out.println("onCharacteristicWrite......receive........==========");
                Log.d("test", "onCharacteristicWrite----");
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    Log.d("test", "BluetoothGatt.GATT_SUCCESS");
                    mOnDataAvailableListener.onCharacteristicWrite(gatt, characteristic);
                }
            }

            @Override
            public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                Log.d("test", "onDescriptorWrite--");
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    Log.d(TAG, "Callback: Wrote GATT Descriptor successfully. " + "descriptor:" + descriptor.getCharacteristic().getUuid());
                } else {
                    Log.d(TAG, "Callback: Error writing GATT Descriptor: " + status + ", descriptor:" + descriptor.getCharacteristic().getUuid());
                }
            }
        };
    }

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.d(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.d(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }
        setLeScanCallback();
        return true;

    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param enabled        If true, enable notification. False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled, ConnectBleServiceInfo serviceInfo) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            Log.d("tets", "tets/....setCharacteristicNotification.........null.......");
            return;
        }
        Log.d("tets", "characteristic.getUuid().toString()????end" + characteristic.getUuid().toString());
        boolean flv = mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
        Log.d("tets", "notification success??::::::" + flv);
        if (serviceInfo.getCharateReadUUID().equals(characteristic.getUuid().toString())
                || serviceInfo.getCharateUUID().equals(characteristic.getUuid().toString())) {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID
                    .fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
            descriptor.setValue(serviceInfo.getConectModel());
            if (mBluetoothGatt.writeDescriptor(descriptor)) {
                Log.d("tets", " mBluetoothGatt.writeDescriptor==true");
            } else {
                Log.d("tets", " mBluetoothGatt.writeDescriptor==false");
            }
        }
    }

    /**
     * /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }
        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                return true;

            } else {
                return false;
            }
        }
        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(mContext, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");

        connectingDevice = device.getName();//?????????????
        mBluetoothDeviceAddress = address;//??????????????
        return true;
    }

    /**
     * ??????
     *
     * @param mDeviceAddress
     */
    public synchronized void setBLEService(String mDeviceAddress) {
        System.out.println("mScanning.1..:" + mScanning);
        if (mScanning) {
            scanLeDevice(false);
        }
        System.out.println("isOnServiceConnected 1..." + isOnServiceConnected);
        if (!isOnServiceConnected) {
            isOnServiceConnected = connect(mDeviceAddress);
            System.out.println("isOnServiceConnected 2..." + isOnServiceConnected);
        }
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    public void writeCharacteristic(byte[] data) {
        Log.d("test", "writeCharacteristic");
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.d(TAG, "BluetoothAdapter not initialized");
            return;
        }
        BluetoothGattService myBluetoothGattService = null;
        myBluetoothGattService = mBluetoothGatt.getService(UUID.fromString(kServiceUUID));
        if (myBluetoothGattService != null) {
            BluetoothGattCharacteristic mBluetoothGattCharacteristic = null;
            mBluetoothGattCharacteristic = myBluetoothGattService.getCharacteristic(UUID
                    .fromString(kReadUUID));
            mBluetoothGattCharacteristic.setValue(data);
            mBluetoothGattCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
            mBluetoothGatt.writeCharacteristic(mBluetoothGattCharacteristic);
            Log.d("test", "writeCharacteristic::succed::");
        } else {

            Log.d("test", "writeCharacteristic::myBluetoothGattService == null");
        }
    }

    private byte[] crcCheck(byte[] datas) {
        // crc???
        if (datas == null || datas.length < 3) {
            return null;
        }
        byte crc = 0x00;
        Log.d("test", "send-----------start-");
        for (int i = 1; i < datas.length - 1; i++) {
            Log.d("test", "send????" + datas[i]);
            crc += datas[i];
        }
        Log.d("test", "send-----------end-");
        Log.d("test", "send crc????" + crc);
        datas[datas.length - 1] = crc;
        return datas;
    }


    public void writeCharacteristic_wbp(byte[] data) {
        Log.w("test", "writeCharacteristic");
        if (mBluetoothAdapter == null || mBluetoothGatt == null || data == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        if (Constant.BLT_WBP.equals(connectingDevice) || Constant.BLT_WF1.equals(connectingDevice) || Constant.AL_WBP.equals(connectingDevice)) {
            BluetoothGattService myBluetoothGattService = null;
            myBluetoothGattService = mBluetoothGatt.getService(UUID.fromString(SampleGattAttributes.SeviceIDfbb0));
            BluetoothGattCharacteristic mBluetoothGattCharacteristic = null;
            mBluetoothGattCharacteristic = myBluetoothGattService.getCharacteristic(UUID.fromString(SampleGattAttributes.GetCharacteristicIDfbb2));
            mBluetoothGattCharacteristic.setValue(crcCheck(data));
            mBluetoothGatt.writeCharacteristic(mBluetoothGattCharacteristic);
        }
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;
        return mBluetoothGatt.getServices();
    }

    private void displayGattServices(List<BluetoothGattService> gattServices, ConnectBleServiceInfo serviceInfo) {
        if (gattServices == null)
            return;
        String uuid = null;
        Log.e("displayGattServices", serviceInfo.toString());
        for (BluetoothGattService gattService : gattServices) {
            uuid = gattService.getUuid().toString();
            Log.e("uuid", "" + gattService.getUuid().toString());
            if (serviceInfo.getServiceUUID().equals(uuid)) {
                List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
                for (final BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                    uuid = gattCharacteristic.getUuid().toString();
                    if (uuid.equals(serviceInfo.getCharateReadUUID())) {
                        Log.e("????GattCharacteUuid", uuid + ", CharacteSize: " + gattCharacteristics.size());
                        setCharacteristicNotification(gattCharacteristic, true, serviceInfo);
                        readCharacteristic(gattCharacteristic);
                        Log.e("--------1------", "11");
                        return;
                    }
                    if (uuid.equals(serviceInfo.getCharateUUID())) {
                        Log.e("????GattCharacteUuid", uuid + ", CharacteSize: " + gattCharacteristics.size());
                        setCharacteristicNotification(gattCharacteristic, true, serviceInfo);
                        return;
                    }
                }
            }

        }

    }

    public static long strtoul(String str,int base){
        return strtoul((str+"\0").toCharArray(),base);
    }

    private static long strtoul(char[] cp,int base){
        long result=0,value;
        int i=0;
        if(base==0){
            base=10;
            if(cp[i]=='0'){
                base=8;
                i++;
                if(Character.toLowerCase(cp[i])=='x'&&isxdigit(cp[1])){
                    i++;
                    base=16;
                }
            }
        }else if(base==16){
            if(cp[0]=='0'&&Character.toLowerCase(cp[1])=='x')
                i+=2;
        }
        while(isxdigit(cp[i])&&(value = isdigit(cp[i]) ? cp[i]-'0' : Character.toLowerCase(cp[i])-'a'+10) < base){
            result=result*base+value;
            i++;
        }
        return result;
    }

    private static boolean isxdigit(char c){
        return ('0' <= c && c <= '9')||('a' <= c && c <= 'f')||('A' <= c && c <= 'F');
    }
    private static boolean isdigit(char c){
        return '0' <= c && c <= '9';
    }
}
