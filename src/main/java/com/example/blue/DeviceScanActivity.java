/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.blue;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bluetoothlibrary.BluetoothLeClass;
import com.example.bluetoothlibrary.Impl.ResolveWbp;
import com.example.bluetoothlibrary.SettingUtil;
import com.example.bluetoothlibrary.entity.ConnectBleServiceInfo;
import com.example.bluetoothlibrary.entity.Peripheral;
import com.example.bluetoothlibrary.entity.SycnBp;

import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * Activity for scanning and displaying available Bluetooth LE devices.
 */
public class DeviceScanActivity extends Activity implements AdapterView.OnItemClickListener {
    private final static String TAG = DeviceScanActivity.class.getSimpleName();
    private final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 0;
    public static int WBPMODE = -1;//

    private BluetoothAdapter mBluetoothAdapter;

    private boolean isActivityFront = true;
    public static final int MACRO_CODE_1 = 1;
    public static final int MACRO_CODE_2 = 2;
    public static final int MACRO_CODE_3 = 3;//��ʾ�¶�
    public static final int MACRO_CODE_4 = 4;//
    public static final int MACRO_CODE_5 = 5;
    public static final int MACRO_CODE_6 = 6;
    public static final int MACRO_CODE_7 = 7;
    public static final int MACRO_CODE_12 = 12;
    public static final int MACRO_CODE_13 = 13;
    public static final int MACRO_CODE_15 = 15;
    public static final int MACRO_CODE_16 = 16;
    public static final int MACRO_CODE_18 = 18;
    public static final int MACRO_CODE_19 = 19;
    public static final int MACRO_CODE_20 = 20;
    public static final int MACRO_CODE_23 = 23;
    private final int REQUEST_ENABLE_BT = 0xa01;
    public static boolean isHasPermissions = false;

    private BluetoothLeClass mBLE;
    public static ArrayList<Peripheral> preipheralCopy = new ArrayList<Peripheral>();

    protected Handler handler;
    public ListView listView, datalist;
    private Button button, button_time, start_wbp, user_send;
    private TextView temp_Blt, testtime, error, bettray, tatol, temp_blan, wf;
    private TextView sys, dia, hr, isguestmode, bp_mesure;
    public double temp;
    public LeDeviceListAdapter mLeDeviceListAdapter;

    private ConnectBleServiceInfo connectServiceInfo;
    public int j = 2;
    public boolean openble = true;
    WbpDatalistAdapter wbpDatalistAdapter;
    ResolveWbp resolveWbp = new ResolveWbp();

    //
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        if (Integer.valueOf(android.os.Build.VERSION.SDK) >= 23) {
            getBLEPermissions();
        }
        initview();
        initListener();

        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                openble = false;
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                enableBtIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);

            }

        }
        mBLE = new BluetoothLeClass(this);
        mBLE.setBluetoothGattCallback();
        if (!mBLE.initialize()) {
            Log.e(TAG, "Unable to initialize Bluetooth");
            finish();
        }

        if (mBluetoothAdapter.isEnabled()) {
            mBLE.scanLeDevice(true);//start to scan

        }
        // set callback function
        mBLE.setOnsetDevicePreipheral(mOnSetDevicePreipheral);
        mBLE.setOnDataAvailableListener(mOnDataAvailable);
        resolveWbp.setOnWBPDataListener(onWBPDataListener);
        initHandler();

    }

    private void getBLEPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            isHasPermissions = false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
                Toast.makeText(this, "shouldShowRequestPermissionRationale", Toast.LENGTH_SHORT).show();
            }
        } else {
            isHasPermissions = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == -1) {
            isHasPermissions = false;
        } else {
            isHasPermissions = true;
        }

    }


    public void initview() {
        button = $(R.id.sycn);
        button_time = $(R.id.sycn_time);
        temp_blan = $(R.id.temp_blan);
        testtime = $(R.id.time);
        error = $(R.id.error);
        bettray = $(R.id.bettray);
        wf = $(R.id.wf);
        tatol = $(R.id.total);
        //Ѫѹ�Ƶ�����չ
        //��ʾѪѹ���
        sys = $(R.id.sys_id);
        dia = $(R.id.dia_id);
        hr = $(R.id.hr_id);
        isguestmode = $(R.id.isguestmode_id);
        bp_mesure = $(R.id.bp_mesure);
        start_wbp = $(R.id.wbp_start);
        user_send = $(R.id.user_send);
        listView = $(R.id.devive_list);//this for device
        datalist = $(R.id.datas);//this is for sync data from wbp
        temp_Blt = $(R.id.temp);
        temp_Blt.setVisibility(View.INVISIBLE);
        listView.setOnItemClickListener(this);

    }

    public void initListener() {

        //this is to get data .
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start to sync data
                resolveWbp.SendForAll(mBLE);
            }
        });
        button_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start to sync time
                resolveWbp.getNowDateTime(mBLE);
            }
        });

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }
        //  wbp start measure and stop measure;
        start_wbp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("....", "start test");
                if (j % 2 == 0) {
                    resolveWbp.onSingleCommand(mBLE);
                } else {
                    resolveWbp.onStopBleCommand(mBLE);
                }
                j++;
            }
        });

        // wbp to change user
        user_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                j++;
                if (j % 2 == 0) {
                    SettingUtil.userModeSelect = 1;
                } else {
                    SettingUtil.userModeSelect = 2;
                }


                resolveWbp.sendUserInfoToBle(mBLE);
            }
        });

    }


    private void initHandler() {
        handler = new Handler() {

            @Override
            public void dispatchMessage(Message msg) {
                switch (msg.what) {
                    case MACRO_CODE_1://show the device list
                        listView.setAdapter(mLeDeviceListAdapter);
                        if (mLeDeviceListAdapter != null) {
                            mLeDeviceListAdapter.notifyDataSetChanged();
                        }
                        break;
                    case MACRO_CODE_2:
                        listView.setAdapter(mLeDeviceListAdapter);
                        if (mLeDeviceListAdapter != null) {
                            mLeDeviceListAdapter.notifyDataSetChanged();
                        }
                        break;
                    case MACRO_CODE_3://wt1 temp
                        listView.setVisibility(View.GONE);
                        temp_Blt.setVisibility(View.VISIBLE);
                        temp = (Double) msg.obj;
                        temp_Blt.setText("" + formatDouble4(temp));
                        break;
                    case MACRO_CODE_4://temp state high
                        listView.setVisibility(View.GONE);
                        temp_Blt.setVisibility(View.VISIBLE);
                        temp_Blt.setText("temp high");
                        break;
                    case MACRO_CODE_5://temp state low
                        listView.setVisibility(View.GONE);
                        temp_Blt.setVisibility(View.VISIBLE);
                        temp_Blt.setText("temp low");
                        break;
                    case MACRO_CODE_6://wt2 unblance templistView.setVisibility(View.GONE);
                        temp_Blt.setVisibility(View.VISIBLE);
                        temp = (Double) msg.obj;
                        temp_Blt.setText("" + formatDouble4(temp));
                        break;
                    case MACRO_CODE_7://wt2 balance temp
                        listView.setVisibility(View.GONE);
                        temp_Blt.setVisibility(View.VISIBLE);
                        bettray.setText("" + msg.arg1);
                        temp = (Double) msg.obj;
                        temp_blan.setText("" + formatDouble4(temp));
                        break;
                    case MACRO_CODE_12://Cuff pressure
                        listView.setVisibility(View.GONE);
                        bp_mesure.setText("" + msg.arg1);
                        break;
                    case MACRO_CODE_13://web the final result
                        listView.setVisibility(View.GONE);
                        sys.setText("" + msg.arg1);
                        dia.setText("" + msg.arg2);
                        hr.setText("" + msg.obj);
                        String str2 = msg.getData().getString("isguestmode");
                        isguestmode.setText("" + str2);
                        break;
                    case MACRO_CODE_15://error
                        listView.setVisibility(View.GONE);
                        error.setText("" + msg.obj);
                    case MACRO_CODE_16://sync total datas
                        listView.setVisibility(View.GONE);
                        datalist.setAdapter(wbpDatalistAdapter);
                        if (wbpDatalistAdapter != null) {
                            wbpDatalistAdapter.notifyDataSetChanged();
                        }
                        tatol.setText("" + msg.arg1);
                        break;
                    case MACRO_CODE_18://BTtime
                        listView.setVisibility(View.GONE);
                        Log.e("time", "" + msg.obj);
                        testtime.setText("" + msg.obj);
                        break;
                    case MACRO_CODE_19://battery
                        listView.setVisibility(View.GONE);
                        Log.e("battery", "" + msg.obj);
                        bettray.setText("" + msg.obj);
                        break;
                    case MACRO_CODE_20://fetal wf100
                        listView.setVisibility(View.GONE);
                        Log.e("fetal", "" + msg.arg1);
                        wf.setText("" + msg.arg1);
                        break;
                    default:

                        break;
                }
            }
        };
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!openble) {
            mBLE.scanLeDevice(true);
        }
        Log.e("see?", "" + isActivityFront);
        isActivityFront = true;
    }

    @Override
    protected void onPause() {

        super.onPause();
        isActivityFront = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBLE.disconnect();
        mBLE.close();
        if (mLeDeviceListAdapter != null) {
            mLeDeviceListAdapter.clear();
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == listView) {
            final Peripheral device = mLeDeviceListAdapter.getDevice(position);
            if (device == null) {
                return;
            } else {
                mBLE.setBLEService(device.getPreipheralMAC());
            }
        }
    }


    BluetoothLeClass.OnsetDevicePreipheral mOnSetDevicePreipheral = new BluetoothLeClass.OnsetDevicePreipheral() {
        @Override
        public void setDevicePreipheral(BluetoothDevice device, int model, String SN, float protocolVer) {
            Peripheral preipheral = new Peripheral();
            preipheral.setBluetooth(device.getName());
            preipheral.setPreipheralMAC(device.getAddress());
            switch (model) {
                case 51:
                    preipheral.setModel("WBP202");
                    WBPMODE = 1;
                    break;
                case 57:
                    preipheral.setModel("WBP204");
                    WBPMODE = 1;
                    break;
                default:
                    break;
            }
            //it is just for wbp
            if (WBPMODE != -1) {
                preipheral.setWebMode(WBPMODE);
            }
            preipheral.setPreipheralSN(SN);
            preipheral.setName("Smart thermometer");
            preipheral.setBrand("Wearcare");
            preipheral.setManufacturer("blt");
            preipheral.setProtocolVer(protocolVer);
            preipheral.setRemark("");
            synchronized (preipheralCopy) {
                if (preipheralCopy.size() == 0) {
                    preipheralCopy.add(preipheral);
                    mLeDeviceListAdapter = new LeDeviceListAdapter(DeviceScanActivity.this, preipheralCopy);
                    sendMsg(DeviceScanActivity.MACRO_CODE_1, handler, null);
                } else {
                    boolean isTrue = false;//
                    for (int i = 0; i < preipheralCopy.size(); i++) {
                        Peripheral preipheral3 = preipheralCopy.get(i);
                        if (preipheral3.getPreipheralSN().equals(SN)) {
                            isTrue = true;//����
                        }
                    }
                    //
                    if (!isTrue) {
                        preipheralCopy.add(preipheral);
                        mLeDeviceListAdapter = new LeDeviceListAdapter(DeviceScanActivity.this, preipheralCopy);

                        sendMsg(DeviceScanActivity.MACRO_CODE_2, handler, null);
                    }
                }
                Log.e("the connecting devie", preipheral.toString());
            }
        }
    };

    public ResolveWbp.OnWBPDataListener onWBPDataListener = new ResolveWbp.OnWBPDataListener() {
        @Override
        public void onMeasurementBp(int temp) {
            //�����ʵʱ�����ѹ
            Message msg = new Message();
            msg.what = MACRO_CODE_12;
            msg.arg1 = temp;
            handler.sendMessage(msg);
        }

        @Override
        public void onMeasurementfin(final int SYS, final int DIA, final int PR, final Boolean isguestmode) {
            //����ǲ��������Ľ��

            Log.e("SYS...................", "" + SYS);
            Log.e("DIA...................", "" + DIA);
            Log.e("PR....................", "" + PR);
            Log.e("isguestmode..........", "" + isguestmode);
            Message msg = new Message();
            msg.what = MACRO_CODE_13;
            msg.arg1 = SYS;
            msg.arg2 = DIA;
            msg.obj = PR;
            Bundle bundle = new Bundle();
            bundle.putString("isguestmode", "" + isguestmode);
            msg.setData(bundle);
            handler.sendMessage(msg);
        }

        /**
         * when measure failed
         * @param obj
         */
        @Override
        public void onErroer(Object obj) {
            //����Ǵ�����ʾ
            Log.e("error", "" + obj);

            Message message = new Message();
            message.what = MACRO_CODE_15;
            message.obj = obj;
            handler.sendMessage(message);
        }

        /**
         *
         * @param btbattey  betttey
         * @param bleState   o: spare 1:working
         * @param version
         * @param devState  workmode
         */
        @Override
        public void onState(int btbattey, String bleState, String version, String devState) {
            Log.e("bleState.............", "" + bleState);
            Log.e("btbattey.............", "" + btbattey);
            Log.e("devState.............", "" + devState);
            Log.e("version.............", "" + version);
            Message msg = new Message();
            msg.arg1 = btbattey;
            Bundle bundle = new Bundle();
            bundle.putString("bleState", "" + bleState);
            bundle.putString("version", "" + version);
            bundle.putString("devState", "" + devState);

            msg.setData(bundle);
            handler.sendMessage(msg);


        }

        /**
         * the data that is not send to the app,when bluetooth is reconnect ,it will coming here.
         * @param sycnBps
         */
        @Override
        public void onSycnBp(ArrayList<SycnBp> sycnBps) {

            for (SycnBp sycnBp : sycnBps) {
                Log.e("the sync data ", "sys" + sycnBp.getSys() + "dia" + sycnBp.getDia() + "pr" + sycnBp.getHr() + "time" + sycnBp.getTime());
            }
            wbpDatalistAdapter = new WbpDatalistAdapter(DeviceScanActivity.this, sycnBps);
            Message message = new Message();
            message.arg1 = sycnBps.size();
            message.what = MACRO_CODE_16;
            handler.sendMessage(message);

        }

        @Override
        public void onTime(String wbp_time) {
            Message message = new Message();
            message.obj = wbp_time;
            message.what = MACRO_CODE_18;
            handler.sendMessage(message);

        }

        /**
         *
         * @param user
         */
        @Override
        public void onUser(int user) {
            Log.e("user::", "......" + user);
            Message message = new Message();
            message.arg1 = user;
            message.what = MACRO_CODE_23;
            handler.sendMessage(message);

        }
    };

    /**
     *
     */
    private BluetoothLeClass.OnDataAvailableListener mOnDataAvailable = new BluetoothLeClass.OnDataAvailableListener() {
        /**
         * all data comes from here
         */
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic) {
            final byte[] data = characteristic.getValue();
            resolveWbp.resolveBPData2(data, mBLE, DeviceScanActivity.this);
        }

        /**
         * get the callback from write
         */
        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt,
                                          BluetoothGattCharacteristic characteristic) {

            Log.e(TAG, "onCharWrite " + gatt.getDevice().getName()
                    + " write "
                    + characteristic.getUuid().toString()
                    + " -> "
                    + characteristic.getValue().toString());
        }
    };

    //  you can copy this to your project..if it is works.you will get some datas

    public <T> T $(int id) {
        return (T) findViewById(id);
    }

    //send data
    public static void sendMsg(int flag, Handler handler, Object object) {
        Message msg = new Message();
        msg.what = flag;
        msg.obj = object;
        if (handler != null) {
            handler.sendMessage(msg);
        }
    }

    public static String formatDouble4(double d) {
        DecimalFormat df = new DecimalFormat("#.00");
        return df.format(d);
    }
}





